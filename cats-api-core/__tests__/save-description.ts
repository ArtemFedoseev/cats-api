import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

let catName = 'Котеечкаы';

const cats: CatMinInfo[] = [{ name: catName, description: '', gender: "male" }];

let catId: number;
let incorrectId = "буквы"

const HttpClient = Client.getInstance();

let catDescription = "Самое обычное описание самого обычного котика";

describe('Добавление описанию котику', () => {
    beforeAll(async () => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: {cats},
            });
            if ((add_cat_response.body as CatsList).cats[0].id) {
                catId = (add_cat_response.body as CatsList).cats[0].id;
            } else throw new Error('Не получилось получить id тестового котика!');
        } catch (error) {
            throw new Error('Не удалось создать первого котика для автотестов!');
        }
    });

    afterAll(async () => {
        await HttpClient.delete(`core/cats/${catId}/remove`, {
            responseType: 'json',
        });
    });



    it('Добавление корректного описания коту', async () => {
        const response = await HttpClient.post(`core/cats/save-description`, {
            responseType: 'json',
            json: {
                "catId": catId,
                "catDescription": catDescription
            },
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body).toMatchObject({
                description: catDescription,
                id: catId,
                name: catName,
                tags: null,
                likes: 0,
                dislikes: 0,

        });
    });

    it('Добавление описания коту по некорректному ID', async () => {
        await expect(HttpClient.post(`core/cats/save-description`, {
                responseType: 'json',
                json: {
                    "catId": incorrectId,
                    "catDescription": catDescription
                },
            })
        ).rejects.toThrowError('Response code 400 (Bad Request)');
    });
});