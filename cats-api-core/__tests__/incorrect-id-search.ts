import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

let catName = 'БедныйУдаляющийсяКотеечка';

const cats: CatMinInfo[] = [{ name: catName, description: '', gender: "male" }];

let catId: number;
const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

describe('Поиск котика по несуществующему ID', () => {
    beforeAll(async () => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: { cats },
            });
            if ((add_cat_response.body as CatsList).cats[0].id) {
                catId = (add_cat_response.body as CatsList).cats[0].id;
            } else throw new Error('Не получилось получить id тестового котика!');
        } catch (error) {
            throw new Error('Не удалось создать котика для автотестов!');
        }

        await HttpClient.delete(`core/cats/${catId}/remove`, {
            responseType: 'json',
        });
    });

    it('Поиск котика по несуществующему ID', async () => {
        await expect(HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
            responseType: 'json'})
        ).rejects.toThrowError('Response code 404 (Not Found)');
    });

    it('Поиск котика по некорректному ID', async () => {
        await expect(HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
            responseType: 'json'})
        ).rejects.toThrowError('Response code 400 (Bad Request)');
    });
});
