import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types'
import { matchers } from 'jest-json-schema';
expect.extend(matchers);

let catName = 'Котеечка';

const cats: CatMinInfo[] = [{ name: catName, description: '', gender: "male" }];

let limit = 5;
let order = "desc";
let gender = "female";

const idealJson = {
        "type": "object",
        "properties": {
        "groups": {
            "type": "array",
                "items": {
                "type": "object",
                    "properties": {
                    "title": {
                        "type": "string"
                    },
                    "cats": {
                        "type": "array",
                            "items": {
                            "type": "object",
                                "properties": {
                                "id": {
                                    "type": "number"
                                },
                                "name": {
                                    "type": "string"
                                },
                                "description": {
                                    "type": ["string", "null"]
                                },
                                "tags": {},
                                "gender": {
                                    "type": "string"
                                },
                                "likes": {
                                    "type": "number"
                                },
                                "dislikes": {
                                    "type": "number"
                                },
                                "count_by_letter": {
                                    "type": "string"
                                }
                            },
                            "required": [
                                "id",
                                "name",
                                "tags",
                                "gender",
                                "likes",
                                "dislikes",
                                "count_by_letter"
                            ]
                        }
                    },
                    "count_in_group": {
                        "type": "number"
                    },
                    "count_by_letter": {
                        "type": "number"
                    }
                },
                "required": [
                    "title",
                    "cats",
                    "count_in_group",
                    "count_by_letter"
                ]
            }
        },
        "count_output": {
            "type": "number"
        },
        "count_all": {
            "type": "number"
        }
    },
    "required": [
        "groups",
        "count_output",
        "count_all"
    ]
};

const HttpClient = Client.getInstance();

describe('Проверка метода allByLetter', () => {

    it('Проверка структуры ответа', async () => {
        const response = await HttpClient.get(`core/cats/allByLetter?limit=${limit}&order=${order}&gender=${gender}`, {
            responseType: 'json'
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body).toMatchSchema(idealJson);
    });
});